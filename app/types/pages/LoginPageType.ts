export type LoginPageType = {
  handleChangeUserName: (text: string) => void;
  handleChangePassword: (text: string) => void;
  handleLogin: () => void;
  errorUsername: string
  errorPassword: string
  loading: boolean
};
