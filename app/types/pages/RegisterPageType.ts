export type RegisterPageType = {
  handleChangeUserName: (text: string) => void;
  handleChangePassword: (text: string) => void;
  handleChangeConfirmPassword: (text: string) => void;
  handleRegister: () => void;
  errorPassword: string
  errorUsername: string
  loading: boolean
};
