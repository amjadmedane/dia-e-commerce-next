"use client"

import { memo, useEffect, useState } from "react";
// import dynamic from 'next/dynamic'

import { Button } from "../components/Button";
import { InputBox } from "../components/InputBox";
import { Text } from "../components/Text";
import { RegisterPageType } from "../types/pages/RegisterPageType";
// const InputBox = dynamic(() => import('../components/InputBox'), { ssr: false })

const RegisterComponent: React.FC<RegisterPageType> = memo(({
  handleChangeUserName,
  handleChangePassword,
  handleChangeConfirmPassword,
  handleRegister,
  errorPassword,
  errorUsername,
  loading,
}) => {

  // const dispatch = useAppDispatch();

  // useEffect(() => {
  //   dispatch(registerAsync())
  // }, [])

  return (
    <section id="login-page-styles">
      <div className="login-box">
        <Text textAlign={"center"} content="Register 👋" />
        <br />
        <InputBox
          type="text"
          placeholder="Username"
          handleChange={(value: string) => handleChangeUserName(value)}
          error={errorUsername}
        />
        <InputBox
          type="password"
          placeholder="Password"
          handleChange={(value: string) => handleChangePassword(value)}
          error={errorPassword}
        />
        <InputBox
          type="password"
          placeholder="Confirm Password"
          handleChange={(value: string) => handleChangeConfirmPassword(value)}
        />
        <Button loading={loading}
          onClick={handleRegister}
        >
          Sign Up
        </Button>
      </div>
    </section>
  );
});

export default RegisterComponent