"use client"

import { useState } from "react";
import RegisterComponent from "./component"
import { RegisterPageType } from "../types/pages/RegisterPageType";
import { useAppDispatch } from "@/lib/hooks";
import { registerAsync } from "@/lib/features/app/appAPI";
import { useRouter } from 'next/navigation'


const validatePassword = (password: string, confirmPassword: string) => {
  if (password.length < 6) return "6 characters (the more, the merrier! 😄"
  else if (confirmPassword != password) return "password not match"
  return ""
}
const validateUsername = (username: string) => {
  if (username.length < 5) return "5 characters (the more, the merrier! 😄"
  return ""
}


const page = () => {
  const dispatch = useAppDispatch();
  const router = useRouter()


  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const [errorPassword, setErrorPassword] = useState("");
  const [errorUsername, setErrorUsername] = useState("");

  const loading = false

  const handleChangeUserName = (text: string) => setUsername(text);
  const handleChangePassword = (text: string) => setPassword(text);
  const handleChangeConfirmPassword = (text: string) => setConfirmPassword(text);


  const handleRegister = async () => {

    if (validatePassword(password, confirmPassword) == ""
      && validateUsername(username) == ""
    ) {


      dispatch(registerAsync({ username, password }, router))

      setErrorUsername(validateUsername(username));
      setErrorPassword(validatePassword(password, confirmPassword));


    } else {
      setErrorUsername(validateUsername(username));
      setErrorPassword(validatePassword(password, confirmPassword));
    }
  };

  const props: RegisterPageType = {
    handleChangeUserName,
    handleChangePassword,
    handleChangeConfirmPassword,
    handleRegister,
    errorPassword,
    errorUsername,
    loading
  };

  return <RegisterComponent {...props} />


}

export default page