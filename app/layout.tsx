import Image from "next/image";
import type { ReactNode } from "react";
import { StoreProvider } from "./StoreProvider";
import { Nav } from "./components/Nav";

import "./styles/globals.scss";
import styles from "./styles/layout.module.css";
import "./styles/components/button-styles.scss"
import "./styles/components/inputbox-styles.scss"
import "./styles/components/text-styles.scss"

import "./styles/pages/login-page-styles.scss"

interface Props {
  readonly children: ReactNode;
}

export default function RootLayout({ children }: Props) {
  return (
    <StoreProvider>
      <html lang="en">
        <body>
            {/* <Nav /> */}

           

            <main className={styles.main}>{children}</main>

           
        </body>
      </html>
    </StoreProvider>
  );
}
