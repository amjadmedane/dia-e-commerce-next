"use client"

import { useState } from "react";
import LoginComponent from "./component"
import { useAppDispatch } from "@/lib/hooks";
import { loginAsync, registerAsync } from "@/lib/features/app/appAPI";
import { useRouter } from 'next/navigation'
import { LoginPageType } from "../types/pages/LoginPageType";


const validatePassword = (password: string,) => {
  if (password.length < 6) return "6 characters (the more, the merrier! 😄"
  return ""
}

const validateUsername = (username: string) => {
  if (username.length < 5) return "5 characters (the more, the merrier! 😄"
  return ""
}


const LoginPage = () => {
  const dispatch = useAppDispatch();
  const router = useRouter()


  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const [errorUsername, setErrorUsername] = useState("");
  const [errorPassword, setErrorPassword] = useState("");

  const loading = false

  const handleChangeUserName = (text: string) => setUsername(text);
  const handleChangePassword = (text: string) => setPassword(text);


  const handleLogin = async () => {

    if (validatePassword(password) == ""
      && validateUsername(username) == ""
    ) {


      dispatch(loginAsync({ username, password }, router))

      setErrorUsername(validateUsername(username));
      setErrorPassword(validatePassword(password));


    } else {
      setErrorUsername(validateUsername(username));
      setErrorPassword(validatePassword(password));
    }
  };

  const props: LoginPageType = {
    handleChangeUserName,
    handleChangePassword,
    handleLogin,
    errorUsername,
    errorPassword,
    loading
  };

  return <LoginComponent {...props} />


}

export default LoginPage