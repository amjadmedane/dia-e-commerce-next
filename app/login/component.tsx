"use client"

import { memo } from "react";
import { Button } from "../components/Button";
import { InputBox } from "../components/InputBox";
import { Text } from "../components/Text";
import { LoginPageType } from "../types/pages/LoginPageType";

const LoginComponent: React.FC<LoginPageType> = memo(({
  handleChangeUserName,
  handleChangePassword,
  handleLogin,
  errorPassword,
  errorUsername,
  loading,
}) => {

  return (
    <section id="login-page-styles">
      <div className="login-box">
        <Text textAlign={"center"} content="Welcome Back 👋" />
        <br />
        <InputBox
          type="text"
          placeholder="Username"
          handleChange={(value: string) => handleChangeUserName(value)}
          error={errorUsername}
        />
        <InputBox
          type="password"
          placeholder="Password"
          handleChange={(value: string) => handleChangePassword(value)}
          error={errorPassword}
        />

        <Button loading={loading}
          onClick={handleLogin}
        >
          Sign in
        </Button>
      </div>
    </section>
  );
});

export default LoginComponent