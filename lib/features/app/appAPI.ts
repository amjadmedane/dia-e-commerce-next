import { AppDispatch } from '@/lib/store';
import { api } from '../../helper/api'
import { endpoints } from '@/constant/endpoints';
import { UserType } from '@/app/types/data/UserType';
import { appSlice } from './appSlice';


export const registerAsync = (values: UserType, router: any) =>
    async (dispatch: AppDispatch) => {
        api
            .post(endpoints.register, values, { headers: { "Content-Type": "application/json", "Accept": '*/*' } })
            .then((response) => {
                if (response.data.success) {
                    dispatch(appSlice.actions.register(response.data.data));
                    router.push('/')
                } else {
                    alert(response.data.message)
                }
            })
            .catch((error) => {
                if (error?.response?.data?.message)
                    alert(error.response.data.message)
                else {
                    alert("internal server error")
                }
            })
            .finally(() => {
                // dispatch(changeStatusSync(false));
            });
    };


export const loginAsync = (values: UserType, router: any) => async (dispatch: AppDispatch) => {
    api
        .post(endpoints.login, values, { headers: { "Content-Type": "application/json", "Accept": '*/*' } })
        .then((response) => {
            if (response.data.success) {
                dispatch(appSlice.actions.login(response.data.data));
                router.push('/')
            } else {
                alert(response.data.message)
            }
        })
        .catch((error) => {
            if (error?.response?.data?.message)
                alert(error.response.data.message)
            else {
                alert("internal server error")
            }
        })
        .finally(() => {
            // dispatch(changeStatusSync(false));
        });

}

