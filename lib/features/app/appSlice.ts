import { cookiesKey } from "@/constant/cookiesKey";
import { createAppSlice } from "@/lib/createAppSlice";
import { Cookies } from "react-cookie";

export interface AppSliceState {
  user: any,
  token: string
}

const initialState: AppSliceState = {
  user: {},
  token: ""
};

// If you are not using async thunks you can use the standalone `createSlice`.
export const appSlice = createAppSlice({
  name: "app",
  initialState,
  reducers: (create) => ({

    register: create.reducer((state, action: any) => {
      const cookies = new Cookies();
      cookies.set(cookiesKey.token, action.payload.token);

      state.user = action.payload.user
      state.token = action.payload.token
    }),

    login: create.reducer((state, action: any) => {
      const cookies = new Cookies();
      cookies.set(cookiesKey.token, action.payload.token);

      
      state.user = action.payload.user
      state.token = action.payload.token
    })
  }),
  selectors: {
    selectUser: (app) => app.user,
    selectToken: (app) => app.token,
  },
});

// Action creators are generated for each case reducer function.
export const { register, login } =
  appSlice.actions;

export const { selectUser, selectToken } = appSlice.selectors;
